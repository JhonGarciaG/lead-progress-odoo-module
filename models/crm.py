# -*- coding: utf-8 -*-
"""
A extension from the CRM Odoo module
"""

from odoo import models, fields

class CrmLead(models.Model):
    """
    Crm class : a personalization from the CRM Odoo module,  according the customer requeriments
    """
    _inherit = "crm.lead"
    _name = "crm.lead"

    stage_ids = fields.One2many('lead_progress.stage', 'lead_id')