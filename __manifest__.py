# -*- coding: utf-8 -*-
{
    'name': "Lead Progress",
    'version': '1.0-dev',
    'author': "jgarcia@lsv-tech.com",
    'website': 'https://www.lsv-tech.com',
    'category': 'Operations/Project',
    'summary': "Module to set a stage in a lead, and set the progress in terms of percentage",
    'depends':['crm'],
    'data':[],
    'installable': True,
    'auto_install':False,
    'application': False,
    'license': 'LGPL-3'
}
