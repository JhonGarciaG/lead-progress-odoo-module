# -*- coding: utf-8 -*-
"""
All models from stage model, in the lead progress module
"""
from odoo import models, fields

class Stage(models.Model):
    """
    Stage class : to assign stage in a lead of CRM module
    """
    _name = "lead_progress.stage"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    description = fields.Char(string='Description', required=True)
    progress = fields.Float(string="Progress")
    lead_id = fields.Many2one('crm.lead', string='Stage',required=True)
