# -*- conding: utf-8 -*-
"""

"""
from odoo import models, fields

class Category(models.Model):
    """
    Category Class
    """
    _name = "lead_progress.category"
    _rec_name = ""

    name_category = fields.Char(string='Name', required=True)